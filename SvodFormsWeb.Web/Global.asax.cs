﻿using System;
using System.Configuration;
using System.Web.Configuration;
using System.Web;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Web;
using DevExpress.Web;
using Xafari;
using Xafari.Web;

namespace SvodFormsWeb.Web
{
    public class Global : System.Web.HttpApplication
    {
        public Global()
        {
            InitializeComponent();
        }

        protected void Application_Start(Object sender, EventArgs e)
        {
            SvodFormsWebWebApplicationHost
                .SetInstance(new SvodFormsWebWebApplicationHost())
                .Setup();
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
            SvodFormsWebWebApplicationHost.Instance.StartApplication(Session);
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
        }

        protected void Application_EndRequest(Object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            SvodFormsWebWebApplicationHost.HandleException();
        }

        protected void Session_End(Object sender, EventArgs e)
        {
            SvodFormsWebWebApplicationHost.DisposeApplication(Session);
        }

        protected void Application_End(Object sender, EventArgs e)
        {
            SvodFormsWebWebApplicationHost.DisposeInstance();
        }

        #region Web Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }

        #endregion
    }
}
