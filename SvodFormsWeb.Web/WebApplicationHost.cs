﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Templates;
using Xafari;
using Xafari.Web;

namespace SvodFormsWeb.Web
{
    public class SvodFormsWebWebApplicationHost : XafariWebApplicationHost
    {
        public SvodFormsWebWebApplicationHost()
        {
            this.UseSharedApplication = true;
            this.SwitchToNewStyle = true;
            //this.PreferredApplicationWindowTemplateType = TemplateType.Horizontal;
        }

        protected override void OnApplicationCreated(XafariWebApplication application)
        {
            base.OnApplicationCreated(application);
            // add here code to customize application after it is created
        }

        protected override void OnApplicationInitialized(XafariWebApplication application)
        {
            base.OnApplicationInitialized(application);
            // add here code to customize application after it is initialized by XafariApplicationContext
        }

        protected override void OnApplicationStarted(XafariWebApplication application)
        {
            base.OnApplicationStarted(application);
            // add here code to customize application after it is started
        }

        protected override void SetupCore()
        {
            base.SetupCore();
            // add here code to customize application environmemt
        }

        protected override void HandleExceptionCore(XafariWebApplication application, Exception exception)
        {
            base.HandleExceptionCore(application, exception);
            // add here code to customize exception handling
        }
    }
}